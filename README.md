# Map Octree

A fun little mod for Minetest that helps store and retrieve node (block) data from specific areas of the game world in memory (RAM). It utilizes a tree-based data structure called an octree to represent the world in a hierarchical way. The main goal is to provide a more efficient way to handle node data in memory compared to using a flat array. Note that this is currently just a personal project, and I don't actively maintain it for general use.

## Purpose

The primary purpose of this mod is to optimize how node data is stored and accessed in memory for a given area of the game world. By using an octree data structure, the mod can store the node data in a hierarchical and compressed manner, resulting in a smaller memory footprint than just storing the data in a flat array.

It can be useful if you need to keep in LUA memory a map backup that you need to always be able to access (to implement an asynchronous restore system for example).

An additional benefit of the octree structure is that it can be easily serialized and deserialized, allowing for efficient storage and retrieval of the node data to and from disk if needed.

## How it Works

### Octree Data Structure

The core of the mod is the octree data structure, which is a tree-based spatial partitioning technique. In an octree, the 3D space is recursively subdivided into eight octants (cubes), with each node in the tree representing one of these octants. This subdivision continues until a specified level of detail is reached or until the octant contains only a single type of node (a leaf node).

The octree structure allows for efficient storage and retrieval of node data, as only the relevant portions of the tree need to be loaded or processed at any given time.

### Nodes Merging and Palette Generation

To further optimize the storage of node data, the mod employs two compression techniques.

1. It merges subtrees composed by identical children into a leaf. It also merges the leaves containing the most common stored node type into the root.
2. It generates a palette of unique node types present in the octree and assigns shorter codes (indices, literally shorter numbers, we're on LUA after all) to the more frequently occurring node types. This palette is then used to represent the node data in the octree, reducing the overall memory requirements. This reduces disk space, allowing me to save the names as numbers.


## Play around with this

This funcs are temporary but are the key main ones to play around with this:

- `map_octree.save_map_nodes(pos1, pos2)`: Loads the node data of the area between `pos1` and `pos2` from the game world, constructs the octree structure, and stores it in memory (in the world folder as "worldoctree.txt", requires my other mod Fantasy Brawl, since I'm lazy and I reused a function).
- `oct_node.get_node_id(octree, searched_pos)`: Retrieves the node ID (palette index) at the specified `searched_pos` within the given `octree` structure.
