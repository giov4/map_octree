map_octree = {}
local srcpath = minetest.get_modpath("map_octree") .. "/src"



dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/debug.lua")

dofile(srcpath .. "/octcache.lua")
dofile(srcpath .. "/octchunk.lua")
dofile(srcpath .. "/matrix3d.lua")
dofile(srcpath .. "/octmap.lua")
dofile(srcpath .. "/store_map.lua")



function what()
	local pos = worldedit.pos1["Giov4"]
	minetest.log("Searching for node in pos ".. minetest.pos_to_string(pos))
	octmap.get_node_name(st, pos)
end