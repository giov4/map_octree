minetest.register_entity("map_octree:wf", {
	initial_properties = {
		visual = "cube",
		textures = {
			"mapoct_wireframe.png","mapoct_wireframe.png","mapoct_wireframe.png",
			"mapoct_wireframe.png","mapoct_wireframe.png","mapoct_wireframe.png"
		},
		use_texture_alpha = true,
		static_save = false,
		visual_size = {x = 64, y = 64, z = 64}
	},
	timer = 0,
	on_step = function(self, dtime, moveresult)
		self.timer = self.timer + dtime
		if self.timer > 600 then
			self.object:remove()
		end
	end
})


minetest.register_entity("map_octree:node", {
	initial_properties = {
		visual = "cube",
		textures = {
			"mapoct_wireframe.png","mapoct_wireframe.png","mapoct_wireframe.png",
			"mapoct_wireframe.png","mapoct_wireframe.png","mapoct_wireframe.png"
		},
		use_texture_alpha = true,
		static_save = false,
		visual_size = {x = 1, y = 1, z = 1}
	},
	timer = 0,
	on_step = function(self, dtime, moveresult)
		self.timer = self.timer + dtime
		if self.timer > 5 then
			self.object:remove()
		end
	end
})



function add_wireframe_entity(pos, size)
	local e = minetest.add_entity(pos, "map_octree:wf")
	if size then
		e:set_properties({visual_size = {x = size, y = size, z = size}})
	end
end


function add_node_entity(pos)
	minetest.add_entity(pos, "map_octree:node")
end
