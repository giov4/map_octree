
function table.map(t, f)
	for k, v in pairs(t) do
		t[k] = f(v)
	end
end



function table.count(t)
	local c = 0
	for k, v in pairs(t) do
		c = c + 1
	end
	return c
end



function map_octree.round_to_pow_of_2(n)
	local mul = 2
	while mul < n do
		mul = mul * 2
	end

	return mul
end



function map_octree.get_chunk_center(pos)
	local chunk_size = 16
	local half_chunk_size = chunk_size / 2
	return {
		x = math.floor(pos.x / chunk_size) * chunk_size + half_chunk_size,
		y = math.floor(pos.y / chunk_size) * chunk_size + half_chunk_size,
		z = math.floor(pos.z / chunk_size) * chunk_size + half_chunk_size
	}
end



function map_octree.get_chunk_pos(pos)
	local chunk_size = 16
	return {
		x = math.floor(pos.x / chunk_size) * chunk_size,
		y = math.floor(pos.y / chunk_size) * chunk_size,
		z = math.floor(pos.z / chunk_size) * chunk_size
	}
end
