matrix3d = {}



function matrix3d.new(x, y, z, default_value)
	if not default_value then default_value = false end

	local matrix = {}

	for i = 1, x do
		matrix[i] = {}
		for j = 1, y do
			matrix[i][j] = {}
			for k = 1, z do
				matrix[i][j][k] = default_value
			end
		end
	end

	matrix.size = vector.new(x, y, z)

	return matrix
end


function matrix3d:get(x, y, z)
	return (self[x] and self[x][y] and self[x][y][z]) or nil
end


function matrix3d.iterate(matrix, f)  -- f(map, x, y, z)
	for i = 1, #matrix do
		for j = 1, #matrix[i] do
			for k = 1, #matrix[i][j] do
				f(i, j, k)
			end
		end
	end
end