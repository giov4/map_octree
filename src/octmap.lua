octmap = {}



function octmap.new(minp, maxp)
	local map = {}
	minp, maxp = vector.sort(minp, maxp)
	minp, maxp = octchunk.snap_to_center(minp), octchunk.snap_to_center(maxp)

	minetest.load_area(vector.subtract(minp, 64), vector.add(maxp, 64))

	map.minp = minp
	map.maxp = maxp
	map.trees = matrix3d.new(
		math.floor((maxp.x - minp.x) / octchunk.SIZE) + 1,
		math.floor((maxp.y - minp.y) / octchunk.SIZE) + 1,
		math.floor((maxp.z - minp.z) / octchunk.SIZE) + 1
	)

	minetest.log("minp "..minetest.pos_to_string(minp))
	minetest.log("maxp "..minetest.pos_to_string(maxp))
	minetest.log("matrix size "..minetest.pos_to_string(map.trees.size))

	-- 0 = minp
	matrix3d.iterate(map.trees, function(x, y, z)
		local tree_center = vector.new(
			minp.x + (x-1)*octchunk.SIZE,
			minp.y + (y-1)*octchunk.SIZE,
			minp.z + (z-1)*octchunk.SIZE
		)
		tree_center = octchunk.snap_to_center(tree_center)

		if not map.trees[x][y][z] then
			map.trees[x][y][z] = octchunk.new(tree_center)
			add_wireframe_entity(tree_center)
		end
	end)

	return map
end



function octmap.get_node_name(map, pos)
	local tree_snapped_pos = octchunk.snap_to_center(pos)

	local dist_from_min_tree = vector.floor(vector.new(
		(tree_snapped_pos.x - map.minp.x) / 64,
		(tree_snapped_pos.y - map.minp.y) / 64,
		(tree_snapped_pos.z - map.minp.z) / 64
	))

	minetest.log("pos -> tree_snapped_pos "..minetest.pos_to_string(tree_snapped_pos))
	minetest.log("dist_from_min_tree in idx "..minetest.pos_to_string(dist_from_min_tree))
	minetest.log("min_tree "..minetest.pos_to_string(map.minp))

	return octchunk.get_node_name(map.trees[1 + dist_from_min_tree.x][1 + dist_from_min_tree.y][1 + dist_from_min_tree.z], pos)
end



function octmap.serialize(map)
	map = table.copy(map)

	-- serialize all trees
	matrix3d.iterate(map.trees, function(x, y, z)
		map.trees[x][y][z] = octchunk.serialize(map.trees[x][y][z])
	end)

	return minetest.compress(minetest.serialize(map), "zstd", 9)
end



function octmap.deserialize(string)
	local map = minetest.deserialize(minetest.decompress(string, "zstd"))

	-- deserialize all trees
	matrix3d.iterate(map.trees, function(x, y, z)
		map.trees[x][y][z] = octchunk.deserialize(map.trees[x][y][z])
	end)

	return map
end



function octmap.read_from_file(name)
	local file = io.open(name, "rb")
	if file then
		local map = octmap.deserialize(file:read("*all"))
		file:close()
		return map
	end
	minetest.log("file doesn't exist")
	return nil
end