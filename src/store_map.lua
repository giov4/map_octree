mapfile = minetest.get_worldpath().."_octree.txt"

function map_octree.save_map_nodes(pos1, pos2)
	if false then
		local pl_pos = minetest.get_player_by_name("Giov4"):get_pos()
		pos1 = vector.add(pl_pos, -500)
		pos2 = vector.add(pl_pos, 500)
		pos1.y = pl_pos.y-128
		pos2.y = pl_pos.y+128
	else
		pos1 = worldedit.pos1["Giov4"]
		pos2 = worldedit.pos2["Giov4"]
	end

	local map = octmap.new(pos1, pos2)

	minetest.safe_file_write(mapfile, octmap.serialize(map))

	st = map

	return map
end

sm = map_octree.save_map_nodes
