octchunk = {SIZE = 64}

local get_name_from_content_id = minetest.get_name_from_content_id
local tsort = table.sort
local tmap = table.map
local tkey_value_swap = table.key_value_swap
local vsubtract = vector.subtract
local vadd = vector.add
local compress = minetest.compress
local serialize = minetest.serialize
local decompress = minetest.decompress
local deserialize = minetest.deserialize

local ID = 1
local CHILDREN = 2



-- Remove redundancies from a tree if the leaves are the same
local function merge_nodes(octnode)
	local children = octnode[CHILDREN]

	if children then
		local skipped_children = false
		local nodes_count = {}
		local most_common_node

		-- find the most common node
		for i, node in pairs(children) do
			nodes_count[node[ID]] = (nodes_count[node[ID]] or 0) + 1
		end

		for node, count in pairs(nodes_count) do
			most_common_node = most_common_node or node

			if count > nodes_count[most_common_node] then
				most_common_node = node
			end
		end

		-- set it as the representative node of the tree (root)
		octnode[ID] = most_common_node

		-- remove leaves that are the same as the representative
		for i, node in pairs(children) do
			if not node[CHILDREN] and node[ID] == octnode[ID] then -- same leaf
				children[i] = nil
			else
				skipped_children = true
			end
		end

		-- remove empty list
		if not skipped_children then octnode[CHILDREN] = nil end
	end
end



-- Method to determine the index of the subnode based on the position of the object
local function get_pos_idx(oct_node_center, node_pos)
	local index = 1

	if node_pos.x >= oct_node_center.x then index = index + 1 end
	if node_pos.y >= oct_node_center.y then index = index + 2 end
	if node_pos.z >= oct_node_center.z then index = index + 4 end

	return index
end



-- Method to get the center of a subnode based on the index and center of the parent
local function get_child_center(parent_center, parent_size, index)
	local offset = parent_size / 4
	local x, y, z = parent_center.x, parent_center.y, parent_center.z

	if index == 1 then
		x, y, z = x - offset, y - offset, z - offset
	elseif index == 2 then
		x, y, z = x + offset, y - offset, z - offset
	elseif index == 3 then
		x, y, z = x - offset, y + offset, z - offset
	elseif index == 4 then
		x, y, z = x + offset, y + offset, z - offset
	elseif index == 5 then
		x, y, z = x - offset, y - offset, z + offset
	elseif index == 6 then
		x, y, z = x + offset, y - offset, z + offset
	elseif index == 7 then
		x, y, z = x - offset, y + offset, z + offset
	elseif index == 8 then
		x, y, z = x + offset, y + offset, z + offset
	end

	return vector.floor({x = x, y = y, z = z})
end



-- generating palette with a Huffman-like compression
local function create_palette(octnode, counter)
	counter = counter or {}
	local palette = {}

	-- count node occurrences in the tree
	for _, node in pairs(octnode[CHILDREN] or {}) do
		if node[CHILDREN] then
			create_palette(node, counter)
		end
		counter[node[ID]] = (counter[node[ID]] or 0) + 1
	end

	-- generate the palette by assigning smaller indices to more frequent nodes
	if octnode.center then -- tree root
		counter[octnode[ID]] = (counter[octnode[ID]] or 0) + 1

		for node_id, _ in pairs(counter) do
			palette[#palette + 1] = node_id
		end

		tsort(palette, function(a, b)
			return counter[a] > counter[b]
		end)

		tmap(palette, function(node_id)
			return get_name_from_content_id(node_id)
		end)

		palette = tkey_value_swap(palette)

		octnode.palette = palette
	end
end



local function apply_palette(octnode, palette)
	if octnode.center then -- tree root
		palette = octnode.palette
	end

	octnode[ID] = palette[get_name_from_content_id(octnode[ID])]

	for i, node in pairs(octnode[CHILDREN] or {}) do
		apply_palette(node, palette)
	end
end



-- Constructor
function octchunk.new(center_or_pos1, pos2)
	local new_tree = {}
	local center

	if pos2 then
		center_or_pos1, pos2 = vector.sort(center_or_pos1, pos2)
		center = vector.add(center_or_pos1, vector.divide(vector.subtract(pos2, center_or_pos1), 2))
	else
		center = center_or_pos1
	end

	new_tree.center = octchunk.snap_to_center(center) -- Center of the root node
	new_tree.size = octchunk.SIZE                    -- Area size

	--minetest.log("Creating tree: ".. minetest.pos_to_string(new_tree.center).. " ".. new_tree.size)
	octchunk.populate_tree(new_tree)

	return new_tree
end



-- PRECONDITION: does not exceed world limits
local function explore_area(octnode, center, size, area, data)
	if size == 1 then -- single node (-> leaf)
		octnode[ID] = data[area:indexp(center)]
		return
	elseif size == 8 then -- try to check a 8x8x8 area all at once, if all the same, save this as a leaf
		local iterator = area:iterp(vsubtract(center, size / 2), vadd(center, size / 2))
		local original_id, id = data[iterator()], data[iterator()]
		local all_the_same = true

		while id do
			if id ~= original_id then
				all_the_same = false
				break
			end
			id = data[iterator()]
		end

		if all_the_same then
			octnode[ID] = original_id
			return
		end
	end

	octnode[CHILDREN] = {}
	for i = 1, 8 do -- generate children
		octnode[CHILDREN][i] = {}
		explore_area(octnode[CHILDREN][i], get_child_center(center, size, i), size / 2, area, data)
	end

	-- generated all 8 children, merge the same ones
	-- (from the bottom of the tree upwards)
	merge_nodes(octnode)
end



local data = {}
function octchunk.populate_tree(root)
	local min, max = vector.subtract(root.center, root.size / 2), vector.add(root.center, root.size / 2)

	local manip = minetest.get_voxel_manip()
	local emerged_pos1, emerged_pos2 = manip:read_from_map(min, max)
	local area = VoxelArea(emerged_pos1, emerged_pos2)

	manip:get_data(data)

	explore_area(root, root.center, root.size, area, data)

	create_palette(root)
	apply_palette(root)

	octcache.create(root)
	octcache.use(root)

	return root
end



function octchunk.get_node_name(tree, searched_pos, center, size, palette)
	if tree.center then -- the root
		center = tree.center
		size = tree.size
		palette = tree.palette
		af = 0
		add_node_entity(center)
	end

	local index = get_pos_idx(center, vector.floor(searched_pos))

	if not tree[CHILDREN] or not tree[CHILDREN][index] then
		minetest.log("Found leaf node: " .. (table.key_value_swap(palette)[tree[ID]] or "nil"))
		return tree[ID]
	else
		local child_center = get_child_center(center, size, index)
		local child_size = size / 2
		return octchunk.get_node_name(tree[CHILDREN][index], searched_pos, child_center, child_size, palette)
	end
end



function octchunk.snap_to_center(pos)
	local size = octchunk.SIZE
	local half_size = size / 2
	return {
		x = math.floor(pos.x / size) * size + half_size,
		y = math.floor(pos.y / size) * size + half_size,
		z = math.floor(pos.z / size) * size + half_size
	}
end



function octchunk.serialize(octnode)
	return compress(serialize(octnode), "zstd", 9)
end



function octchunk.deserialize(string)
	local tree = deserialize(decompress(string, "zstd"))
	octcache.create(tree)
	octcache.use(tree)
	return tree
end