local ID = 1
local CHILDREN = 2

octcache = {}

local tconcat = table.concat

local trees_cache = {}  -- hash = node
local hashes_refs = {}  -- hash = boolean
local hash_memoization = {}  -- node = hash
local same_hash_replace = {[1] = "", [2] = "w", [3] = "t", [4] = "r", [5] = "f", [6] = "s", [7] = "e", [8] = "i"}

octcache.trees_cache = trees_cache

-- let's make so that the cache automatically avoids growing too big
local cache_info = {
	counter = 0,
	min_refs = 2,							 --
	trim_trigger_size = 200000 -- just for reference
}

setmetatable(trees_cache, {
	__index = cache_info,
  __newindex = function(t, key, value)
		rawset(t, key, value)
		local counter = t.counter
		t.counter = counter + 1

		if (counter + 1) % 200000 == 0 then
			for hash, _ in pairs(t) do
				local hashed_refs = hashes_refs[hash]

				if hashed_refs then
					if hashed_refs < 2 then
						t[hash] = nil
						hashes_refs[hash] = nil
						counter = counter - 1
					end
				end
			end

			t.counter = counter

			minetest.log("trimmed deduplication cache to "..t.counter.." trees")
		end
  end
})



local function generate_hash(octnode)
	if hash_memoization[octnode] then
		return hash_memoization[octnode]
	end

	local children = octnode[CHILDREN]

	if children then
		local compressed_hash = {octnode[ID]}

		local same_block_start
		local last_hash = -1
		local same_amount = 1
		local child

		for i = 1, 8 do
			local children_hash = {}
			child = children[i]

			compressed_hash[i+1] = ""

			if child then
				if child[CHILDREN] then
					hash_memoization[child] = generate_hash(child)
					children_hash[i] = hash_memoization[child]
				else
					children_hash[i] = child[ID]
				end
			else
				children_hash[i] = "x"
			end

			-- compress identical consecutive children
			if children_hash[i] == last_hash then
				same_amount = same_amount + 1
				compressed_hash[same_block_start] = same_hash_replace[same_amount] .. last_hash
			else
				compressed_hash[i+1] = children_hash[i]
				last_hash = children_hash[i]
				same_block_start = i+1
				same_amount = 1
			end
		end

		hash_memoization[octnode] = tconcat(compressed_hash)
		return hash_memoization[octnode]
	else
		hash_memoization[octnode] = octnode[ID]
		return hash_memoization[octnode]
	end
end
octcache.generate_hash = generate_hash



function octcache.create(octnode)
	if trees_cache[generate_hash(octnode)] then return end

	-- caching parent
	trees_cache[generate_hash(octnode)] = octnode

	for i, child in pairs(octnode[CHILDREN] or {}) do
		octcache.create(child)
	end
end



function octcache.use(octnode, iterating)
	local children = octnode[CHILDREN] or {}

	for i, child in pairs(children) do
		local child_hash = generate_hash(child)

		if child[CHILDREN] then  -- recurse on subtree
			octcache.use(child, true)
		end

		children[i] = trees_cache[child_hash]
		hashes_refs[child_hash] = (hashes_refs[child_hash] or 0) + 1
	end

	if not iterating then
		hash_memoization = {}
	end
end


function octcache.delete()
	trees_cache = {}
	hash_memoization = {}
	hashes_refs = {}
end